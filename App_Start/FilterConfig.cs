﻿using System;
using System.Web.Mvc;

namespace trainer4
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters) 
		{ 
			filters.Add(new HandleErrorAttribute()); 
		} 
	}
}

