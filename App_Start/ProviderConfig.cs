﻿using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin;
using System.Web.Helpers;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace trainer4
{
	public partial class Startup
	{
		public void  Configuration(IAppBuilder app)
		{
			ConfigureAuth (app);
		}

		// For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
		void ConfigureAuth (IAppBuilder app)
		{
			// Enable the application to use a cookie to store information for the signed in user
			app.UseCookieAuthentication(new CookieAuthenticationOptions
				{
					AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
					LoginPath = new PathString("/Account/LogOn")
				});

			AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
		}
	}
}

