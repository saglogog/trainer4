﻿using System;
using System.Web.Mvc;
using trainer4.Model;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace trainer4
{
	//[CustomAuthorization(Roles = "admin")]
	public class AdminController: Controller
	{
		public AdminController()
			:this (new UserManager<Users>(new UserRoleStore<Users>()))
		{
		}

		public AdminController(UserManager<Users> userManager)
		{
			UserManager = userManager;
		}

		public UserManager<Users> UserManager { get; private set; }



		public ActionResult ManageUsers()
		{
			return View();
		}


		public ActionResult ManageCourses()
		{
			//if (User.Identity.GetUserId() == null)
			//	// add add custom error information
			//	return View("~/Views/Shared/Error.cshtml");
			//if (UserManager.IsInRoleAsync(User.Identity.GetUserId(), "admin").Result)
			//{
			//	return View();
			//}
			//else
			//	// add custom error information
			//	return View("~/Views/Shared/Error.cshtml");
			return View();
		}

		public ActionResult ListUsers()
		{

			var aulv = new AdminUserListViewModel();

			string msg = "";
			List<AdminUserListViewModel> list = new List<AdminUserListViewModel>();
			try
			{
				list = aulv.GetListOfAllUsers();
			}
			catch (NullReferenceException ex)
			{
				msg = ex.Message + "--REKT--" + ex.Source + "--REKT--" + ex.StackTrace;
			}
			//if (dal.GetListOfAllUsers() != null)
			//{
			//	List<AdminUserListViewModel> list = null;
			//	list = dal.GetListOfAllUsers();
			//}
			//string json = JsonConvert.SerializeObject(list);

			if (list[0] != null)
				return Json(list, JsonRequestBehavior.AllowGet);
			else 
				return View("~/Views/Shared/Error.cshtml");

		}

		public ActionResult GetAddUsersFormHtml()
		{
			return File(Server.MapPath("/Views/Admin/") + "AddUsersForm.cshtml", "text/html");
		}

		/// <summary>
		/// Gets the new user inserted by the administrator in the ~/Views/Admin/AddUsersForm.cshtml.
		/// </summary>
		/// <returns>The new user.</returns>
		/// <param name="user">User.</param>
		[HttpPost]
		public ActionResult GetNewUser(AdminUserAddFormViewModel user)
		{
			if (UserManager.IsInRoleAsync(User.Identity.GetUserId(), "admin").Result)
			{
				try {
					new Helpers().SaveUser(user);
				}
				catch(FormatException e){
					// customize error, don't simply throw the exception, show the error to the user/admin
					throw e;
					//return View("~/Views/Shared/Error.cshtml"); 
				}
				catch(Exception e){
					throw e;
				}
				return Json(new {succ= user});
			}
			return Json(new {err = "or"});
		}

		public ActionResult RemoveUser()
		{
			return View();
		}

	}
}
