﻿using System;
using System.Web.Mvc;
using trainer4.Model;
using System.Web.Helpers;
using Microsoft.AspNet.Identity;

namespace trainer4
{
	[Authorize]
	public class CoursesController: Controller
	{
		public CoursesController()
			:this (new UserManager<Users>(new UserRoleStore<Users>()))
		{
		}

		public CoursesController(UserManager<Users> userManager)
		{
			UserManager = userManager;
		}

		public UserManager<Users> UserManager { get; private set; }

		public ActionResult Course1()
		{
			Contents con = new Contents ();
			con.ch_id = 0;
			con.ct_id = 0;
			con.c_hash = "";
			con.c_path = "https://www.youtube.com/watch?v=4gDch1p4c_M";
			con.c_text = "1. (n.) A sign of virility, strength or social efficacy. An informal measure of one's ability to mack, pimp, or generally to get play, action or get your freak on. Generally characterized as \"strong\" or \"weak.\" \n2. To \"exercise one's pimp hand\": to increase one's skill at short-term sexual interactions, or to demonstrate such. \n3. One's ability to control one's bitches. \n4. A back-handed slap, used to emphasize superiority, or a forthcoming need to choke a bitch.\n1. Damn, son! Yo' ass picked up some *fine* bitches last night! Yo' pimp hand is strong! \n2. You're losing your touch, man. Come out to the club tonight so we can exercise your pimp hand. \n3. His strong pimp hand keeps his hos in line. \n4. You gonna take that shit?! Introduce yo' pimp hand to that bitch ass motherfucker!\n";
			return Json (con, JsonRequestBehavior.AllowGet);
		}
			
		public ActionResult Course2()
		{

			var cs = new ChapterStore<Chapter> (); 

//			Section sect = new Section ();
//			sect.s_desc = "test section added for test purposes";
//			sect.s_name = "test_section_1";
//
//			dal.AddSectionToDb (sect);

			Chapter ch = new Chapter ();
			ch.ch_desc = "chapter inserted from test method";
			ch.ch_name = "test_chapter_1";
			ch.s_id = 1;
			ch.ch_number = 1;

			cs.AddToDb (ch);

			return Json (new {a = 'b'}, JsonRequestBehavior.AllowGet);

		}
	}
}

