﻿using System;
using System.Web.Mvc;

namespace trainer4
{
	public class HomeController:Controller
	{
		public ActionResult Index()
		{
			return View ();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page."; 

			return View(); 
		}


		public ActionResult Courses()
		{
			ViewBag.Message = "Courses Page"; 

			return View(); 
		}

		public ActionResult Tests()
		{
			ViewBag.Message = "Tests Page"; 

			return View(); 
		}


		public ActionResult Contact()
		{
			ViewBag.Message = "Your Contact Page."; 

			return View(); 
		}
	}
}

