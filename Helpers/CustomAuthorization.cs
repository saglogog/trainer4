﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using trainer4.Model;

namespace trainer4
{
	/// <summary>
	/// Custom authorization. It inherits from AuthorizeAttribute, whose code can be found here: https://github.com/ASP-NET-MVC/aspnetwebstack/blob/master/src/System.Web.Mvc/AuthorizeAttribute.cs
	/// Overriding the AuthorizeCore() method instead of OnAuthorization as per suggestion (http://stackoverflow.com/questions/32509273/should-i-call-base-onauthorizationfiltercontext)
	/// Perhaps inherit from IdentityDbContext in the future to save trouble of
	/// custom authorization. (http://www.dotnetcurry.com/aspnet-mvc/1229/user-authentication-aspnet-mvc-6-identity)
	/// 					  (http://stackoverflow.com/questions/19902756/asp-net-identity-dbcontext-confusion)
	/// </summary>
	public class CustomAuthorization: AuthorizeAttribute
	{
		public CustomAuthorization() :this(new RoleManager<Role>(new RoleStore<Role>()), new UserManager<Model.Users>(new UserRoleStore<Users>()))
		{

		}

		public CustomAuthorization(RoleManager<Role> rm, UserManager<Users> um)
		{
			
			RoleManager = rm;
			UserManager = um;
		}

		public RoleManager<Role> RoleManager { get; private set; }
		public UserManager<Users> UserManager { get; private set; }

		protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
		{
			if (httpContext == null)
			{
				throw new ArgumentNullException("httpContext");
			}

			foreach (var role in RoleManager.Roles)
			{
				if (UserManager.IsInRoleAsync(httpContext.User.Identity.GetUserId(), role.Name).Result)
				{
					return true;
				}
			}
			Debug.WriteLine(this.Roles);
			return false;
		}
	}

}

