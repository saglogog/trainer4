﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using trainer4.Model;

namespace trainer4
{
	public class Helpers
	{
		public Helpers()
		{
			UserManager = new UserManager<Users>(new UserRoleStore<Users>());
		}

		public UserManager<Users> UserManager { get; private set; }

		/// <summary>
		/// Saves the user to the database.
		/// </summary>
		/// <param name="viewModelUser">View model user.</param>
		public bool SaveUser(AdminUserAddFormViewModel viewModelUser)
		{
			Task<IdentityResult> ir;
			Task<IdentityResult> ir2 = null;

			try
			{
				Users user = new Users
				{
					UserName = viewModelUser.Username,
					Email = viewModelUser.Email,
					PasswordHash = viewModelUser.Password,
					name = viewModelUser.Name,
					surname = viewModelUser.Surname,
					flag_active = true
				};

				//DataAccessLayer dal = new DataAccessLayer();
				//await dal.CreateUserAsync(user);
				//var user2 = dal.FindUserByName(user.UserName).Result;

				ir = UserManager.CreateAsync(user);
				ir.Wait();
				var user2 = UserManager.FindByNameAsync(viewModelUser.Username).Result;
				// getting the user id after saving the user to the database (need to change this, performance nightmare much? :0)

				// add user to each of the roles assigned
				for (int i = 0; i < viewModelUser.Roles.Count; i++)
				{
					ir2 = UserManager.AddToRoleAsync(user2.Id, viewModelUser.Roles[i]);
					ir2.Wait();
				}


				if (ir.IsCompleted && ir2.IsCompleted)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (FormatException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
