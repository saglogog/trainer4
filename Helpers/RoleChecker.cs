﻿using System;
using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;
using trainer4.Model;

namespace trainer4
{
	public static class RoleChecker
	{
		public static bool IsUserInRole(string id, string role)
		{
			return new UserManager<Users>(new UserRoleStore<Users>()).IsInRole(id, role);
		}
	}
}
