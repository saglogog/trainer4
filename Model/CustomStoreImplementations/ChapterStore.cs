﻿using System;

namespace trainer4.Model
{
	public class ChapterStore<Chapter>: ITrainer4Store<trainer4.Model.Chapter>
	{

		public void AddToDb(trainer4.Model.Chapter ch)
		{
			using (var db = new TrainerDBContext())
			{
				db.Chapters_DbSet.Add(ch);
				db.SaveChanges();
			}
		}
	}
}

