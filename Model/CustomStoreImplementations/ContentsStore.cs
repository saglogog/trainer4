﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace trainer4.Model
{
	public class ContentsStore<Contents>: ITrainer4Store<trainer4.Model.Contents>
	{
		public void AddToDb(trainer4.Model.Contents con)
		{
			using (var db = new TrainerDBContext())
			{
				db.Contents_DbSet.Add(con);
				db.SaveChanges();
			}
		}

		public List<trainer4.Model.Contents> GetContentByChapterId(int ch_id)
		{
			using (var db = new TrainerDBContext())
			{
				var contents =	from a in db.Contents_DbSet
								where (a.ch_id == ch_id)
								select a;
							//db.Contents_DbSet.Find (ch_id);	

								

				return contents.ToList();
			}
		}
	}
}

