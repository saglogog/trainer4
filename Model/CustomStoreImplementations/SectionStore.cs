﻿using System;

namespace trainer4.Model
{
	public class SectionStore<Section>: ITrainer4Store<trainer4.Model.Section>
	{
		public void AddToDb(trainer4.Model.Section sect)
		{
			using (var db = new TrainerDBContext())
			{
				db.Sections_DbSet.Add(sect);
				db.SaveChanges();
			}
		}
	}
}

