﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Data.Entity;
//
//namespace trainer4.Model
//{
	// The DAL file contains the data access layer methods aka the methods that write
	// and read data to and from the database.

//	public class DataAccessLayer
//	{
//		public DataAccessLayer() { }
//
//		TrainerDBContext db;
//		public DataAccessLayer(TrainerDBContext _db)
//		{
//			db = _db;
//		}


//		public List<Contents> GetContentByChapterId(int ch_id)
//		{
//			using (var db = new TrainerDBContext())
//			{
//				var contents = from a in db.Contents_DbSet
//							   where (a.ch_id == ch_id)
//							   select a;
//
//				return contents.ToList();
//			}
//		}

//		public void AddChapterToDb(Chapter ch)
//		{
//			using (var db = new TrainerDBContext())
//			{
//				db.Chapters_DbSet.Add(ch);
//				db.SaveChanges();
//			}
//		}

//		public void AddContentToDb(Contents con)
//		{
//			using (var db = new TrainerDBContext())
//			{
//				db.Contents_DbSet.Add(con);
//				db.SaveChanges();
//			}
//		}

//		public void AddSectionToDb(Section sect)
//		{
//			using (var db = new TrainerDBContext())
//			{
//				db.Sections_DbSet.Add(sect);
//				db.SaveChanges();
//			}
//		}

		#region UserDataAccessMethods
		//Below follow asynchronous methods to save users to the database.
		//These methods are used in the IUserStore implementation class, Trainer4UserStore in the Users.cs file.

//		public async Task CreateUserAsync(Users user)
//		{
//
//			db.Users.Add(user);
//			await db.SaveChangesAsync();
//
//		}

//		public async Task DeleteUserAsync(Users user)
//		{
//
//			db.Users.Remove(user);
//			await db.SaveChangesAsync();
//
//		}

//		public Task<Users> FindUserById(string u_Id)
//		{
//
//			return Task.Run(() => db.Users.Where(arg => arg.u_Id.ToString() == u_Id).Select(arg => arg).FirstOrDefault());
//
//		}

//		public Task<Users> FindUserByName(string username)
//		{
//
//			return Task.Run(() => db.Users.Where(arg => arg.UserName == username).Select(arg => arg).FirstOrDefault());
//
//		}

//		public async Task UpdateUserAsync(Users user)
//		{
//
//			db.Users.Attach(user);
//			db.Entry(user).State = EntityState.Modified;
//			await db.SaveChangesAsync();
//		}
		#endregion


		#region RoleDataAccessMethods
		// Below follow asynchronous methods to save roles to the database.
		// These methods are used in the IRoleStore implementation class, Trainer4RoleStore in the Role.cs file.

//		public IQueryable<Role> GetRoles()
//		{
//			return db.Set<Role>();
//		}

//		public async Task CreateRoleAsync(Role role)
//		{
//			db.Roles.Add(role);
//			await db.SaveChangesAsync();
//		}

//		public async Task DeleteRoleAsync(Role role)
//		{
//			db.Roles.Remove(role);
//			await db.SaveChangesAsync();
//		}

//		public Task<Role> FindRoleById(string r_Id)
//		{
//			return Task.Run(() => db.Roles.Where(arg => arg.r_id.ToString() == r_Id).Select(arg => arg).FirstOrDefault());
//		}

//		public Task<Role> FindRoleByName(string r_name)
//		{
//			return Task.Run(() => db.Roles.Where(arg => arg.Name == r_name).Select(arg => arg).FirstOrDefault());
//		}

//		public async Task UpdateRoleAsync(Role role)
//		{
//			db.Roles.Attach(role);
//			db.Entry(role).State = EntityState.Modified;
//			await db.SaveChangesAsync();
//		}
		#endregion


		#region UserRoleDataAccessMethods
		// User Role database methods follow.

//		public async Task AddUserToRole(Users user, string roleName)
//		{
//			Task<Role> roleTask = FindRoleByName(roleName);
//			await roleTask;
//
//			UserRole ur;
//
//			try
//			{
//				ur = new UserRole
//				{
//					u_id = user.u_Id,
//					r_id = roleTask.Result.r_id
//				};
//			}
//			catch (NullReferenceException ex)
//			{
//				throw new NullReferenceException("The role by the given role name was not found.", ex);
//			}
//
//			db.UserRoles_DbSet.Add(ur);
//			await db.SaveChangesAsync();
//		}

		// the method is declared as async so as to return a Task
//		public async Task<IList<string>> GetUserRoles(Users user)
//		{
//			List<string> usersRoleNames = new List<string>();
//
//			IQueryable<UserRole> urtq = await Task.Run(() => db.UserRoles_DbSet.Where(arg => arg.u_id == user.u_Id).Select(arg => arg));
//			List<UserRole> userRoleTuples = urtq.ToList();
//			foreach (UserRole ur in userRoleTuples)
//			{
//				Role role = await Task.Run(() => db.Roles.Where(arg => arg.r_id == ur.r_id).Select(arg => arg).FirstOrDefault());
//				usersRoleNames.Add(role.Name);
//			}
//
//			return usersRoleNames;
//		}

//		public async Task<bool> DoesUserHaveRole(Users user, string roleName)
//		{
//			// get role w/ the corresponding name
//			Role r = await FindRoleByName(roleName);
//			// create the userrole that will be used later
//			UserRole ur;
//			try
//			{
//				ur = new UserRole
//				{
//					u_id = user.u_Id,
//					r_id = r.r_id
//				};
//			}
//			catch (NullReferenceException ex)
//			{
//				throw new NullReferenceException("The role by the given role name was not found.", ex);
//			}
//			// return true if the userrole with the given users' id and the rolename id is in the db 
//			if (Task.Run(() => db.UserRoles_DbSet.Where(arg => arg.u_id == ur.u_id && arg.r_id == ur.r_id).Select(arg => arg).FirstOrDefault()).Result != null)
//				return true;
//			else return false;
//		}

//		public async Task RemoveUserFromRole(Users user, string roleName)
//		{
//			Role r = await FindRoleByName(roleName);
//
//
//
//			UserRole ur;
//			try
//			{
//				ur = db.UserRoles_DbSet.Where(arg => arg.u_id == user.u_Id && arg.r_id == r.r_id).Select(arg => arg).FirstOrDefault();
//			}
//			catch (NullReferenceException ex)
//			{
//				throw new NullReferenceException("The role by the given role name was not found.", ex);
//			}
//
//			db.UserRoles_DbSet.Remove(ur);
//			await db.SaveChangesAsync();
//		}
		#endregion


		#region ManageUsersDataAccessMethods
		// methods for the use of the Admin Controller, manage users page.

//		public List<AdminUserListViewModel> GetListOfAllUsers()
//		{
//			List<AdminUserListViewModel> query = null;
//			//TrainerDBContext db2 = new TrainerDBContext();
//			try
//			{
//				using (var db = new TrainerDBContext())
//				{
//					query =
//						(from user in db.Users
//						 join ur in db.UserRoles_DbSet on user.u_Id equals ur.u_id
//						 join role in db.Roles on ur.r_id equals role.r_id
//						 select new AdminUserListViewModel { UserId = user.u_Id, UserName = user.UserName, Role = role.Name }).ToList();
//				}
//			}
//			catch (NullReferenceException ex)
//			{
//				//query = new List<AdminUserListViewModel>();
//				//query.Add(new AdminUserListViewModel { UserId = 0, UserName = "Fuck You", Role = "You get no result." });
//				throw ex;
//			}
//
//			//USE VIEWMODEL TO FORMAT AND DISPLAY THE DATA INSTEAD OF ANONYMOUS TYPES!!!
//			return ReformatAndRemoveDuplicates(query);
//		}
		#endregion


		#region Helpers

		/// <summary>
		/// Reformats the given list the and then removes duplicates.
		/// Linq implementation.
		/// Maybe this implementation can be improve to use only one list,
		/// by deleting the duplicates in the original list.
		/// </summary>
		/// <returns>The new reformatted list.</returns>
		/// <param name="list">Unformatted list.</param>
		/// <param name="rolesNumber">Maximum number of roles a user may have.</param>
//		public List<AdminUserListViewModel> ReformatAndRemoveDuplicates(List<AdminUserListViewModel> list)
//		{
//			List<AdminUserListViewModel> formattedList = new List<AdminUserListViewModel>();
//			string[] roles;
//			for (int i = 0; i < list.Count; i++)
//			{
//				if (!formattedList.Exists(x => x.UserId == list[i].UserId))
//				{
//					// using linq to get the roles by iterating over the list since we assume it is small;
//					// if the database grows we 'll have to consider using a dictionary 
//					roles = list.Where(x => x.UserId == list[i].UserId).Select(x => x.Role).ToArray<string>();
//					AdminUserListViewModel user = list[i];
//					// append roles to a single string and assign them to the user object .Role property
//					user.Role = "";
//					for (int j = 0; j < roles.Length; j++)
//					{
//						if (roles[j] == null)
//						{
//							user.Role += " ";
//						}
//						else
//						{
//							user.Role += roles[j] + " ";
//						}
//					}
//					formattedList.Add(user);
//				}
//			}
//
//			return formattedList;
//		}

		#endregion
//	}
//}

