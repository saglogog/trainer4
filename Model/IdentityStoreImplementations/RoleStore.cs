﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace trainer4.Model
{
	/// <summary>
	/// Asynchronous methods to save roles to the database.
	/// </summary>
	public class RoleStore<TRole> : IRoleStore<Role>, IQueryableRoleStore<Role>
	{

//		TrainerDBContext db;
//		public RoleStore(TrainerDBContext _db)
//		{
//			db = _db;
//		}

		public IQueryable<Role> Roles
		{
			get
			{
				using (var db = new TrainerDBContext ()) {
					return db.Set<Role> ();
				}
			}
		}

		public async Task CreateAsync(Role role)
		{
			using (var db = new TrainerDBContext ()) {
				db.Roles.Add (role);
				await db.SaveChangesAsync ();
			}
		}

		public async Task DeleteAsync(Role role)
		{
			using (var db = new TrainerDBContext ()) {
				db.Roles.Remove (role);
				await db.SaveChangesAsync ();
			}
		}

		public async Task<Role> FindByIdAsync(string roleId)
		{
			using (var db = new TrainerDBContext ()) {
				return await Task.Run (() => db.Roles.Where (arg => arg.r_id.ToString () == roleId).Select (arg => arg).FirstOrDefault ());
			}
		}

		public async Task<Role> FindByNameAsync(string roleName)
		{
			using (var db = new TrainerDBContext ()) {
				return await Task.Run (() => db.Roles.Where (arg => arg.Name == roleName).Select (arg => arg).FirstOrDefault ());
			}
		}

		public async Task UpdateAsync(Role role)
		{
			using (var db = new TrainerDBContext ()) {
				db.Roles.Attach (role);
				db.Entry (role).State = EntityState.Modified;
				await db.SaveChangesAsync ();
			}
		}

		public void Dispose()
		{
			//db.Dispose();
		}
	}
}
