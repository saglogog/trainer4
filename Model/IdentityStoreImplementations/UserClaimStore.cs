﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace trainer4.Model
{

	public class UserClaimStore<TUser> : IUserClaimStore<TUser>
		where TUser : Users
	{
		public Task AddClaimAsync(TUser user, Claim claim)
		{
			throw new NotImplementedException();
		}

		public Task CreateAsync(TUser user)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(TUser user)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public Task<TUser> FindByIdAsync(string userId)
		{
			throw new NotImplementedException();
		}

		public Task<TUser> FindByNameAsync(string userName)
		{
			throw new NotImplementedException();
		}

		public Task<IList<Claim>> GetClaimsAsync(TUser user)
		{
			throw new NotImplementedException();
		}

		public Task RemoveClaimAsync(TUser user, Claim claim)
		{
			throw new NotImplementedException();
		}

		public Task UpdateAsync(TUser user)
		{
			throw new NotImplementedException();
		}
	}

}
