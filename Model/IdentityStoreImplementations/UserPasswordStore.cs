﻿using System;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;

namespace trainer4.Model
{
	public class UserPasswordStore<TUser> : IUserPasswordStore<Users>
	{

		public async Task CreateAsync(Users user)
		{
			if (user == null) {
				throw new ArgumentNullException ("user");
			}

			if (user.UserName == null || user.PasswordHash == null) {
				throw new ArgumentNullException ("user.UserName" + " or " + "user.password");
			}

			using (var db = new TrainerDBContext ()) {
				db.Users.Add (user);
				await db.SaveChangesAsync ();
			}
		}

		public async Task DeleteAsync(Users user)
		{
			using (var db = new TrainerDBContext ()) {
				if (user == null) {
					throw new ArgumentNullException ("user");
				}

				db.Users.Remove (user);
				await db.SaveChangesAsync ();
			}
		}

		public async Task<Users> FindByIdAsync(string userId)
		{
			using (var db = new TrainerDBContext ()) {
				return await Task.Run (() => db.Users.Where (arg => arg.u_Id.ToString () == userId).Select (arg => arg).FirstOrDefault ());
			}
		}

		public async Task<Users> FindByNameAsync(string userName)
		{
			if (userName == null)
			{
				throw new ArgumentNullException("userName");
			}
			using (var db = new TrainerDBContext ()) {	
				return await Task.Run (() => db.Users.Where (arg => arg.UserName == userName).Select (arg => arg).FirstOrDefault ());
			}
		}

		public async Task UpdateAsync(Users user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}

			using (var db = new TrainerDBContext ()) {
				db.Users.Attach (user);
				db.Entry (user).State = EntityState.Modified;
				await db.SaveChangesAsync ();
			}
		}

		//implementation of IUserPasswordStore follows:
		//also, tasks do not have to be asynchronous, here we try to implement them synchronously

		public async Task SetPasswordHashAsync(Users user, string passwordHash)
		{
			user.PasswordHash = passwordHash;
			using (var db = new TrainerDBContext ()) {
				await Task.FromResult (0);
			}

		}

		public async Task<string> GetPasswordHashAsync(Users user)
		{
			string passwordHash = user.PasswordHash;
			using (var db = new TrainerDBContext ()) {
				return await Task.FromResult (passwordHash);
			}
		}

		public async Task<bool> HasPasswordAsync(Users user)
		{
			//			if (user.password == null)
			//				return Task.FromResult (false);
			//			else
			//				return Task.FromResult (true);
			using (var db = new TrainerDBContext ()) {
				return user.PasswordHash == null ? await Task.FromResult (false) : await Task.FromResult (true);
			}
		}

		public void Dispose()
		{
			//db.Dispose();
		}
	}
}
