﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;

namespace trainer4.Model
{
	public class UserRoleStore<TUser> : UserStore<Users>, IUserRoleStore<Users>
	{
//		TrainerDBContext db;
//		public UserRoleStore(TrainerDBContext _db) : base(_db)
//		{
//			db = _db;
//		}

		public async Task AddToRoleAsync(Users user, string roleName)
		{
			using (var db = new TrainerDBContext ()) {
				//get role by name
				Task<Role> roleTask = Task.Run (() => db.Roles.Where (arg => arg.Name == roleName).Select (arg => arg).FirstOrDefault ());
				await roleTask;

				UserRole ur;

				try {
					ur = new UserRole {
						u_id = user.u_Id,
						r_id = roleTask.Result.r_id
					};
				} catch (NullReferenceException ex) {
					throw new NullReferenceException ("The role by the given role name was not found.", ex);
				}

				db.UserRoles_DbSet.Add (ur);
				await db.SaveChangesAsync ();
			}
		}

		// the method is declared as async so as to return a Task
		public async Task<IList<string>> GetRolesAsync(Users user)
		{
			List<string> usersRoleNames = new List<string>();

			using (var db = new TrainerDBContext ()) {
				IQueryable<UserRole> urtq = await Task.Run (() => db.UserRoles_DbSet.Where (arg => arg.u_id == user.u_Id).Select (arg => arg));
				List<UserRole> userRoleTuples = urtq.ToList ();
				foreach (UserRole ur in userRoleTuples) {
					Role role = await Task.Run (() => db.Roles.Where (arg => arg.r_id == ur.r_id).Select (arg => arg).FirstOrDefault ());
					usersRoleNames.Add (role.Name);
				}
			}
			return usersRoleNames;
		}

		public async Task<bool> IsInRoleAsync(Users user, string roleName)
		{
			bool isInRole;
			using (var db = new TrainerDBContext ()) {
				// get role w/ the corresponding name
				Role r = await Task.Run (() => db.Roles.Where (arg => arg.Name == roleName).Select (arg => arg).FirstOrDefault ());
				// create the userrole that will be used later
				UserRole ur;
				try {
					ur = new UserRole {
						u_id = user.u_Id,
						r_id = r.r_id
					};
				} catch (NullReferenceException ex) {
					throw new NullReferenceException ("The role by the given role name was not found.", ex);
				}
				// return true if the userrole with the given users' id and the rolename id is in the db 
				if (Task.Run (() => db.UserRoles_DbSet.Where (arg => arg.u_id == ur.u_id && arg.r_id == ur.r_id).Select (arg => arg).FirstOrDefault ()).Result != null)
					isInRole = true;
				else
					isInRole = false;
			}
			return isInRole;
		}

		public async Task RemoveFromRoleAsync(Users user, string roleName)
		{
			using (var db = new TrainerDBContext ()) {
				// get role w/ the corresponding name
				Role r = await Task.Run (() => db.Roles.Where (arg => arg.Name == roleName).Select (arg => arg).FirstOrDefault ());

				UserRole ur;
				try {
					ur = db.UserRoles_DbSet.Where (arg => arg.u_id == user.u_Id && arg.r_id == r.r_id).Select (arg => arg).FirstOrDefault ();
				} catch (NullReferenceException ex) {
					throw new NullReferenceException ("The role by the given role name was not found.", ex);
				}

				db.UserRoles_DbSet.Remove (ur);
				await db.SaveChangesAsync ();
			}
		}
	}
}
