﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace trainer4.Model
{
	public class Chapter
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ch_id { get; set; }

		public int s_id { get; set; }
		[ForeignKey("s_id")]
		public virtual Section SectionId{ get; set; }

		public int ch_number { get; set; }
		public string ch_name { get; set; }
		public string ch_desc { get; set; }
	}
}

