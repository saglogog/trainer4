﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace trainer4.Model
{
	public class ContentType
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ct_id { get; set; }
		public string ct_name { get; set;}
		public string ct_desc { get; set; }
	}
}

