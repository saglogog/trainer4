﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace trainer4.Model
{
	public class Contents
	{

		string _c_path;
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int c_id { get; set; }

		public int ch_id { get; set; }
		[ForeignKey("ch_id")]
		public virtual Chapter ChapterId{ get; set; }

		public int ct_id { get; set; }
		[ForeignKey("ct_id")]
		public virtual ContentType ContentTypeId{ get; set; }


		public string c_text { get; set; }
		public string c_hash { get; set; }
		public string c_path { get{ return CorrectLinkFormat(_c_path); } set{ _c_path = value;} }
	
		/// <summary>
		/// Corrects the link format, to make appropriate for display as an embedded video.
		/// </summary>
		/// <returns>Corrected link format.</returns>
		/// <param name="link">Link</param>
		private string CorrectLinkFormat(string link)
		{
			//for now contains only an implementation for youtube links.
			string ytPattern = @"http[s]?://www.youtube.com/.*";
			Regex ytRg = new Regex (ytPattern);
			string correctedLink = "";

			if(ytRg.IsMatch(link) )
			{
				string[] individualPartsArray = link.Split ('/');
				string videoCode = individualPartsArray [3].Split ('=') [1];
				correctedLink = individualPartsArray[0] + "//" + individualPartsArray[2] + "/embed/" + videoCode;

				return correctedLink;
			}
			else return link;
		}
	}
}

