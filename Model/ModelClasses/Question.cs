﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace trainer4.Model
{
	public class Question
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int q_id{ get; set;}

		public int ch_id { get; set; }
		[ForeignKey("ch_id")]
		public virtual Chapter ChapterId{ get; set; }


		public string q_name { get; set; }
		public string q_text { get; set; }
	}
}

