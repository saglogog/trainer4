﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace trainer4.Model
{
	public class QuestionAnswer
	{
		[Key] 
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int a_id { get; set; }

		public int q_id { get; set; }
		[ForeignKey("q_id")] 
		public virtual Question QuestionId{ get; set; } 

		public string a_text { get; set; }
	}
}

