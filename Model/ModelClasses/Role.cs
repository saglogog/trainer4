﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace trainer4.Model
{
	public class Role : IdentityRole<string, UserRole>, IRole
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int r_id { get; set; }
		public new string Name { get; set; }
		public string r_desc { get; set; }

		// this property is needed to implement the IRole interface
		public new string Id
		{ get { return r_id.ToString(); } }


	}
}

