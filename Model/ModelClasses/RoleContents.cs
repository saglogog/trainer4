﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace trainer4.Model
{
	public class RoleContents
	{
		[Key] 
		[Column(Order=1)]
		public int r_id { get; set; }
		[ForeignKey("r_id")]
		public virtual Role RoleId{ get; set; }

		[Key] 
		[Column(Order=2)]
		public int c_id { get; set; }
		[ForeignKey("c_id")]
		public virtual Contents ContentsId{ get; set; }
	}
}

