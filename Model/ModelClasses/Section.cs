﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace trainer4.Model
{
	public class Section
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int s_id { get; set; }
		public string s_name { get; set; }
		public string s_desc { get; set; }
	}
}

