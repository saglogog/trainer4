﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace trainer4.Model
{
	public class Stats
	{

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int st_id { get; set; }

		public int u_id { get; set; }
		[ForeignKey("u_id")]
		public virtual Users UsersId{ get; set; }

		public int ch_id { get; set; }
		[ForeignKey("ch_id")]
		public virtual Chapter ChapterId{ get; set; }

		public float chapter_test_correct_percentage { get; set; }
		public string current_weaknesses_chapter_numbers { get; set; }

	}
}

