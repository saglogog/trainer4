﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace trainer4.Model
{
	public class UserClaim : IdentityUserClaim<string>
	{
		[Key]
		public override int Id { get; set; }

		public override string ClaimType { get; set; }

		public override string ClaimValue { get; set; }


		public int u_id { get; set; }

		[ForeignKey("u_id")]
		public virtual Users UsersId { get; set; }

		public override string UserId { get; set; }
		//public override string UserId { get { return u_id.ToString(); } set { u_id = Convert.ToInt32(value); } }
	}
}
