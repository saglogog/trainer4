﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace trainer4.Model
                  
{
	public class UserLogin: IdentityUserLogin<string>
	{
		[Key]
		[Column(Order = 1)]
		public int u_id { get; set; }
		[Key]
		[Column(Order = 2)]
		public override string ProviderKey { get; set; }
		[Key]
		[Column(Order = 3)]
		public override string LoginProvider { get; set; }


		[ForeignKey("u_id")]
		public virtual Users UsersId { get; set; }

		public override string UserId{ get; set; }
		//public override string UserId { get { return u_id.ToString(); } set { u_id = Convert.ToInt32(value); } }

	}

}
