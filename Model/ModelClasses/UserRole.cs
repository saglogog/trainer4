﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace trainer4.Model
{
	public class UserRole:IdentityUserRole<string>
	{
		[Key] 
		[Column(Order=1)] 
		public int u_id { get; set; }
		[ForeignKey("u_id")]
		public virtual Users UsersId{ get; set; }

		[Key] 
		[Column(Order=2)] 
		public int r_id { get; set; }
		[ForeignKey("r_id")]
		public virtual new Role RoleId{ get; set; }

		public override string UserId{ get; set; }
		//public override string UserId { get { return u_id.ToString(); } set { u_id = Convert.ToInt32(value); } }
	}
}

