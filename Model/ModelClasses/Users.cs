﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;


namespace trainer4.Model
{
	public class Users: IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
	{
		// the password hash is set in the Trainer4UserStore class by the set passwordhashasync method
		// so there is no need for a custom implementation
//		private string _password;
//		PasswordHasher ph = new PasswordHasher();
//		public string password { get { return _password; } set { _password = ph.HashPassword(value); } }




		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int u_Id{ get; set; }

		public override string Id { get{return u_Id.ToString ();} }
		public override string UserName { get; set; }
		public override string PasswordHash { get; set; }
		public string name { get; set; }
		public string surname { get; set; }
		public bool flag_active { get; set; }
		public override string Email { get; set; }
		public override bool EmailConfirmed { get; set; }
		public override string SecurityStamp { get; set; }
		public override string PhoneNumber { get; set; }
		public override bool PhoneNumberConfirmed { get; set; }
		public override bool TwoFactorEnabled  { get; set; }
		public override DateTime? LockoutEndDateUtc { get; set; }
		public override bool LockoutEnabled { get; set; }
		public override int AccessFailedCount { get; set; }


		//public IdentityUser ToIdentityUser(Users user)
		//{

		//	return new IdentityUser
		//	{
		//		Id = user.u_id.ToString(),
		//		UserName = user.username,
		//		Email = user.mail,
		//		PasswordHash = user.password
		//	};
		//}

	}


}

