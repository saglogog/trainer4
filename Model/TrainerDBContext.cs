﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Diagnostics;

namespace trainer4.Model
{
	/// <summary>
	/// Custom db context that inherits from System.Data.Entity.DbContext and 
	/// maps the application model classes to the database tables.
	/// Perhaps inherit from IdentityDbContext in the future to save trouble of
	/// custom authorization.
	/// </summary>
	public class TrainerDBContext: IdentityDbContext<Users, Role, string, UserLogin, UserRole, UserClaim>, IDisposable
	{
		// creating this custom constructor somehow solves the mysterious 
		// System.Net.Sockets.SocketException : Could not resolve host '.' error.
		// You may comment out the constructor to find out by yourself.
		// (based on http://stackoverflow.com/questions/28531201/entitytype-identityuserlogin-has-no-key-defined-define-the-key-for-this-entit) 
		// The contents of the constructor don't seem to matter at the present moment.
		public TrainerDBContext(): base ("name=TrainerDBContext")
		{
			Database.Log = (x) => Debug.WriteLine(x);
			//Database.SetInitializer<TrainerDBContext>(null);// Remove default initializer
			//Configuration.ProxyCreationEnabled = false;
			//Configuration.LazyLoadingEnabled = false;
		}
		 

		// the sets on which the operations will be performed on by the code
		public DbSet<Chapter> Chapters_DbSet { get; set; }
		public DbSet<Contents> Contents_DbSet { get; set; }
		public DbSet<ContentType> ContentTypes_DbSet { get; set; }
		public DbSet<Question> Questions_DbSet { get; set; }
		public DbSet<QuestionAnswer> QuestionAnswers_DbSet { get; set; }
		public DbSet<RoleContents> RoleContents_DbSet { get; set; }
		public DbSet<Section> Sections_DbSet { get; set; }
		public DbSet<Stats> Stats_DbSet { get; set; }
		public DbSet<UserRole> UserRoles_DbSet { get; set; }
		//public DbSet<UserLogin> UserLogin_DbSet { get; set; }

		public DbSet<UserLogin> UserLogins_DbSet { get; set; }
		public DbSet<UserClaim> UserClaims_DbSet { get; set; }
		//public DbSet<Role> Roles_DbSet { get; set; }
		// we have already used users_dbset in data access classes and the name
		// of the property clarifies its purpose(dbset). So we since there cannot be
		// 2 dbsets that can contain a Users instance (System.InvalidOperationException: Multiple object sets per type are not supported.The object sets 'Users_DbSet' and 'Users' can both contain instances of type 'trainer4.Model.Users'.)
		// we use the Users_DbSet and Roles_DbSet as wrappers for the IdentityContext inherited Users and Roles properties.
		// The wrappers will be use only in the dal anyway.
		//public DbSet Users_DbSet { get { return (DbSet)Users; } set { Users = (IDbSet<Users>)value; } }
		//public DbSet Roles_DbSet { get { return (DbSet)Roles; } set { Roles = (IDbSet<IdentityRole>)value; } }


		/// <summary>
		/// Maps the database tables to the classes.
		/// </summary>
		/// <param name="modelBuilder">Model builder.</param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Chapter>().ToTable("chapter", "public");

			modelBuilder.Entity<Contents>().ToTable("contents", "public");

			modelBuilder.Entity<ContentType>().ToTable("content_type", "public");

			modelBuilder.Entity<Question>().ToTable("question", "public");

			modelBuilder.Entity<QuestionAnswer>().ToTable("question_answer", "public");

			modelBuilder.Entity<Role>().Property(x => x.Name).HasColumnName("r_name");
			modelBuilder.Entity<Role>().ToTable("role", "public");

			modelBuilder.Entity<RoleContents>().ToTable("role_contents", "public");

			modelBuilder.Entity<Section>().ToTable("section", "public");

			modelBuilder.Entity<Stats>().ToTable("stats", "public");

			modelBuilder.Entity<UserRole>().Property(x => x.UserId).HasColumnName("userid");
			modelBuilder.Entity<UserRole>().ToTable("user_role", "public");


			//modelBuilder.Entity<UserLogin>().HasKey<string>(l => l.UserId);

			// column names in users table have different names than the corresponding class proeperties
			// so we map them accordingly
			modelBuilder.Entity<Users>().Property(x => x.AccessFailedCount).HasColumnName("accessfailedcount");
			modelBuilder.Entity<Users>().Property(x => x.LockoutEnabled).HasColumnName("lockoutenabled");
			modelBuilder.Entity<Users>().Property(x => x.LockoutEndDateUtc).HasColumnName("lockoutenddateutc");
			modelBuilder.Entity<Users>().Property(x => x.TwoFactorEnabled).HasColumnName("twofactorenabled");
			modelBuilder.Entity<Users>().Property(x => x.PhoneNumberConfirmed).HasColumnName("phonenumberconfirmed");
			modelBuilder.Entity<Users>().Property(x => x.PhoneNumber).HasColumnName("phonenumber");
			modelBuilder.Entity<Users>().Property(x => x.SecurityStamp).HasColumnName("securitystamp");
			modelBuilder.Entity<Users>().Property(x => x.PasswordHash).HasColumnName("password");
			modelBuilder.Entity<Users>().Property(x => x.EmailConfirmed).HasColumnName("emailconfirmed");
			modelBuilder.Entity<Users>().Property(x => x.Email).HasColumnName("email");
			modelBuilder.Entity<Users>().Property(x => x.UserName).HasColumnName("username");
			modelBuilder.Entity<Users>().Property(x => x.u_Id).HasColumnName("u_id");
			modelBuilder.Entity<Users>().ToTable("users", "public");

			modelBuilder.Entity<UserLogin>().Property(x => x.UserId).HasColumnName("userid");
			modelBuilder.Entity<UserLogin>().Property(x => x.u_id).HasColumnName("u_id");
			modelBuilder.Entity<UserLogin>().Property(x => x.LoginProvider).HasColumnName("loginprovider");
			modelBuilder.Entity<UserLogin>().Property(x => x.ProviderKey).HasColumnName("providerkey");
			modelBuilder.Entity<UserLogin>().ToTable("user_login", "public");

			modelBuilder.Entity<UserClaim>().Property(x => x.UserId).HasColumnName("userid");
			modelBuilder.Entity<UserClaim>().Property(x => x.Id).HasColumnName("id");
			modelBuilder.Entity<UserClaim>().Property(x => x.u_id).HasColumnName("u_id");
			modelBuilder.Entity<UserClaim>().Property(x => x.ClaimType).HasColumnName("claimtype");
			modelBuilder.Entity<UserClaim>().Property(x => x.ClaimValue).HasColumnName("claimvalue");
			modelBuilder.Entity<UserClaim>().ToTable("user_claim", "public");
			// don't use identity keys
			modelBuilder.Conventions.Remove<StoreGeneratedIdentityKeyConvention>();
		}

	}
}

