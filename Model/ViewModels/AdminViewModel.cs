﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System;
using System.Linq;

namespace trainer4.Model
{
	//Admin Controller view models
	public class AdminUserListViewModel
	{
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string Role { get; set; }


		public List<AdminUserListViewModel> GetListOfAllUsers()
		{
			List<AdminUserListViewModel> query = null;
			//TrainerDBContext db2 = new TrainerDBContext();
			try
			{
				using (var db = new TrainerDBContext())
				{
					query =
						(from user in db.Users
							join ur in db.UserRoles_DbSet on user.u_Id equals ur.u_id
							join role in db.Roles on ur.r_id equals role.r_id
							select new AdminUserListViewModel { UserId = user.u_Id, UserName = user.UserName, Role = role.Name }).ToList();
				}
			}
			catch (NullReferenceException ex)
			{
				//query = new List<AdminUserListViewModel>();
				//query.Add(new AdminUserListViewModel { UserId = 0, UserName = "Fuck You", Role = "You get no result." });
				throw ex;
			}

			//USE VIEWMODEL TO FORMAT AND DISPLAY THE DATA INSTEAD OF ANONYMOUS TYPES!!!
			return ReformatAndRemoveDuplicates(query);
		}


		/// <summary>
		/// Reformats the given list the and then removes duplicates.
		/// Linq implementation.
		/// Maybe this implementation can be improve to use only one list,
		/// by deleting the duplicates in the original list.
		/// </summary>
		/// <returns>The new reformatted list.</returns>
		/// <param name="list">Unformatted list.</param>
		/// <param name="rolesNumber">Maximum number of roles a user may have.</param>
		public List<AdminUserListViewModel> ReformatAndRemoveDuplicates(List<AdminUserListViewModel> list)
		{
			List<AdminUserListViewModel> formattedList = new List<AdminUserListViewModel>();
			string[] roles;
			for (int i = 0; i < list.Count; i++)
			{
				if (!formattedList.Exists(x => x.UserId == list[i].UserId))
				{
					// using linq to get the roles by iterating over the list since we assume it is small;
					// if the database grows we 'll have to consider using a dictionary 
					roles = list.Where(x => x.UserId == list[i].UserId).Select(x => x.Role).ToArray<string>();
					AdminUserListViewModel user = list[i];
					// append roles to a single string and assign them to the user object .Role property
					user.Role = "";
					for (int j = 0; j < roles.Length; j++)
					{
						if (roles[j] == null)
						{
							user.Role += " ";
						}
						else
						{
							user.Role += roles[j] + " ";
						}
					}
					formattedList.Add(user);
				}
			}

			return formattedList;
		}
	}

	/// <summary>
	/// This is the model of the form that the administrator uses to add new users in the database;
	/// the form can be viewed at ~/Views/Admin/AddUsersForm.cshtml and is supplied by ajax to the /Admin/ManageUsers
	/// page after the AddUsers control has been clicked..
	/// </summary>
	public class AdminUserAddFormViewModel
	{
		//sample regex pattern shamelessly stolen from http://www.regular-expressions.info/email.html
		//static string emailPattern = @"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}";
		static string emailPattern = @"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}";
		Regex rg = new Regex(emailPattern);
		private string _Email;

		public string Username { get; set; }
		public string Email
		{
			get { return _Email; }
			set
			{
				if (rg.IsMatch(value))
				{
					_Email = value;
				}
				else
				{
					throw new FormatException("The email was not formatted as required.");
				}
			}
		}
		public string Password { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public IList<string> Roles { get; set; }
	}

}
