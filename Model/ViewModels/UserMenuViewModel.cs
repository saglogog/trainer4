﻿using System.Collections.Generic;

namespace trainer4.Model
{
	public class UserMenuViewModel
	{
		public Dictionary<string,string> MenuLinks { get; set; }
	}
}
