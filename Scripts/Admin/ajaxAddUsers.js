﻿var retrievedData, test=false;

$( "#addUsers" ).on ('click', function(){
	var serviceURL = "/Admin/GetAddUsersFormHtml";

	$.ajax({
		method: "GET",
		url: serviceURL,
		dataType: "html",
		data: retrievedData,
		success: appendAddUsersForm,
		error: errorFunc 
	})
});

function appendAddUsersForm(retrievedData){
	$("#usersBox").empty();
	$("#usersBox").append(retrievedData);
	// add focus to username element (the first element of the addUsers form
	$("#username").focus();
}

function errorFunc(jqXHR, textStatus) {
	alert( "Request failed: " + textStatus );
}



//jquery ui form

//https://jqueryui.com/dialog/#modal-form

//$( function() {

//	// variables' declaration
//	var dialog, form,

//		name = $("#name"),
//		email = $("#email"),
//		password = $("#password"),
//		allFields = $( [] ).add( name ).add( email ).add( password ),
//		tips = $(".validateTips");

//	function updateTips(t){
//		tips
//			.text(t)
//			.addClass("ui-state-highlight")
//		setTimeout(function(){
//			tips.removeClass("ui-state-highlight", 1500);
//		}, 500);
//	}

//	// function that sends the data to the backend
//	function addUser(){}

//	// dialog window initialization
//	dialog = $("#dialog-form").dialog({
//		autoOpen: false,
//		height: 400,
//		width: 350,
//		modal: true,
//		buttons:{
//			"Create User": addUser,
//			Cancel: function(){
//				dialog.dialog("close");
//			}
//		},
//		close: function(){
//			form[0].reset();
//			allFields.removeClass("ui-state-error");
//		}
//	});

//	// open the dialog when the element #addUsers (the button) is clicked
//	$("#addUsers").on('click', function(){
//		dialog.dialog("open");
//	});
//});