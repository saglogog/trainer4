﻿
// this particular syntax is used so that we can submit the form that is loaded by ajax in the
// users box well 
$(document).on('submit', 'form#addUsersForm', function(event){

	// new json object containing the values of the form
	var user = new Object();
	user.username = $('#username').val();
	user.email = $('#email').val();
	user.password = $('#password').val();
	user.name = $('#name').val();
	user.surname = $('#surname').val();
	user.roles = $('#roles').val();

	var userString = JSON.stringify(user);
	var serviceURL = "/Admin/GetNewUser";

	$.ajax({
		method: "POST",
		url: serviceURL,
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		data: userString,
		success: function(msg){
			console.log("success:", msg, userString);
			},
		error: function(xhr, status, error){
			console.log("error: ", xhr.responseText, error);
		}
	})

	//console.log(user.username, user.email, user.password, user.name, user.surname, user.roles)

	// show the now updated list of users
	$('#listUsers').trigger("click");
	// do not refresh the page
	return false;
});