﻿var retrievedData;

$( "#Course1" ).click(function (){
			
	var serviceURL = "/Courses/" + $(this).attr("id");

	//jquery ajax() method
	$.ajax({
		type: "GET",
		url: serviceURL,
		contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: retrievedData,
        success: successFunc,
        //perhaps remove to make js unobtrusive???
        error: errorFunc	
	})
});

function successFunc(retrievedData){
	$("#courseBox").append(retrievedData.c_text);
	$("#courseBox").append("<iframe src=" + retrievedData.c_path +"></iframe>");
	//document.body += retrievedData.c_text;
}

function errorFunc(jqXHR, textStatus) {
	alert( "Request failed: " + textStatus );
}
