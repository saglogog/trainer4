﻿var retrievedData;

$( "#listUsers" ).click(function (){
	var serviceURL = "/Admin/ListUsers" 

	$.ajax({
		method: "GET",
		url: serviceURL,
		//contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: retrievedData,
		success: successFunc,
		error: errorFunc
	})
	//console.log("listUsers Clicked")
});

function successFunc(retrievedData){
	// this check is used so that the data isn't appended twice
	$("#usersBox").empty();
	var table = '<table class="table table-hover table-responsive"><thead><tr><th>User Id</th><th>Username</th><th>User Role</th></tr></thead><tbody>';

	for(var i = 0; i < retrievedData.length; i++){
		table += '<tr class = "clickable-row">';
		table += '<td class = "user-id">' + retrievedData[i].UserId + "</td>";
		table += '<td class = "username">' + retrievedData[i].UserName + "</td>";
		table +='<td class = "user-role">' + retrievedData[i].Role + "</td>";
		table += "</tr>";
	}

	table += "</tbody></table>";

	$("#usersBox").append(table);
	test = true;
	//$("#usersBox").append(retrievedData[0].Role);
	//$("#courseBox").append("<iframe src=" + retrievedData.c_path +"></iframe>");
	//console.log("Entered successFunc")
	
}

function errorFunc(jqXHR, textStatus) {
	alert( "Request failed: " + textStatus );
}
