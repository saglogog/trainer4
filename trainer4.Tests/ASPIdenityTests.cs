﻿using System;
using NUnit.Framework;
using trainer4.Model;
using System.Security.Claims;
using trainer4;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace trainer4.Tests
{

	[TestFixture()]	
	public class ASPIdenityTests
	{

		TrainerDBContext db;

		[SetUp]
		public void Init()
		{
			TrainerDBContext _db = new TrainerDBContext();
			db = _db;
		}

		[TearDown]
		public void Dispose()
		{
			db.Dispose();
		}

		[Test()]
		public void TestRoles(){
			UserManager<Users> um = new UserManager<Users> (new UserRoleStore<Users>());

//			PasswordHasher ph = new PasswordHasher ();
//			Users user = new Users {
//				UserName = "saglogog2",
//				PasswordHash = ph.HashPassword("1231853211"),
//				name = "sag",
//				surname = "logog",
//				flag_active = true,
//				Email = "jingerbread@yahoo.com",
//				EmailConfirmed = true,
//				SecurityStamp = null,
//				PhoneNumber = null,
//				PhoneNumberConfirmed = false,
//				TwoFactorEnabled = false,
//				LockoutEndDateUtc = null, 
//				LockoutEnabled = false,
//				AccessFailedCount = 0
//			};
//
//			if (user != null) {
//				try {
//					um.Create (user);
//				} catch (Exception e) {
//					throw e;
//				}
//			}
//
			um.AddToRole (um.FindByName ("saglogog2").Id, "admin");

			//Console.WriteLine(um.GetClaims (um.FindByName ("saglogog2").Id));

			Assert.True(um.IsInRole (um.FindByName ("saglogog2").Id, "admin"));
			//AccountController ac = new AccountController ();
			//ac.ControllerContext.HttpContext ();
			//ac.User.IsInRole ("admin");
		}

	}
}

