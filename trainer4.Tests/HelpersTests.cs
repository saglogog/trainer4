﻿using System;
using NUnit.Framework;
using trainer4;
using trainer4.Model;
using System.Collections.Generic;

namespace trainer4.Tests
{	
	[TestFixture()]
	public class HelpersTests
	{
		[Test()]
		public void SaveUserTest()
		{
			AdminUserAddFormViewModel user = new AdminUserAddFormViewModel
			{
				Username = "fellos3",
				Password = "123456789",
				Email = "pollo@loco.mx",
				Name = "hermano",
				Surname = "nero",
				Roles = new List<string>(new string[] { "teacher", "student" })
			};
			Helpers h = new Helpers();
			bool completed = h.SaveUser(user);

			if (completed == true)
			{
				Assert.Pass("Tasks Completed");
			}
			else
			{
				Assert.Fail();
			}
		}
	}
}
