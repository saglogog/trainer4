﻿using System;
using NUnit.Framework;
using trainer4.Model;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace trainer4.Tests
{
	[TestFixture ()]
	public class AdminUserListViewModelTests
	{

		[Test()]
		public void TestGetListOfAllUsers()
		{
			AdminUserListViewModel aulv = new AdminUserListViewModel ();

			var userList = aulv.GetListOfAllUsers();

			foreach (var user in userList)
			{
				Console.WriteLine(user.UserName + user.Role + user.UserId);
			}
		}

		[Test()]
		public void TestReformatAndRemoveDuplicates()
		{

			List<AdminUserListViewModel> testList1 = new List<AdminUserListViewModel> ();
			// creation of aulv object and first addition to the list
			AdminUserListViewModel aulv = new AdminUserListViewModel{
				UserId = 1,
				UserName = "test1",
				Role = "teacher"
			};
			testList1.Add (aulv);
			// second addition to the list with the same id
			aulv = new AdminUserListViewModel {
				UserId = 1,
				UserName = "test1",
				Role = "student"
			};
			testList1.Add (aulv);
			// 3rd addition
			aulv = new AdminUserListViewModel {
				UserId = 1,
				UserName = "test1",
				Role = "admin"
			};
			testList1.Add (aulv);
			// addition w/ new id & username
			aulv = new AdminUserListViewModel {
				UserId = 2,
				UserName = "test2",
				Role = "teacher"
			};
			testList1.Add (aulv);
			// 2nd addition w/ new ...
			aulv = new AdminUserListViewModel {
				UserId = 2,
				UserName = "test2",
				Role = "student"
			};
			testList1.Add (aulv);
			// another new addition
			aulv = new AdminUserListViewModel {
				UserId = 3,
				UserName = "test3",
				Role = "student"
			};
			testList1.Add (aulv);

			//Console.WriteLine (aulv.UserId + " " + aulv.UserName + " " + aulv.Role);

			List<AdminUserListViewModel> testList2 = aulv.ReformatAndRemoveDuplicates (testList1);
			foreach (AdminUserListViewModel aulv2 in testList1) {
				Console.WriteLine (aulv2.UserId + " " + aulv2.UserName + " " + aulv2.Role);
				//Console.WriteLine ();
			}
			Console.Write ("\n\n");
			foreach (AdminUserListViewModel aulv2 in testList2) {
				Console.WriteLine (aulv2.UserId + " " + aulv2.UserName + " " + aulv2.Role);
			}
		}
	}
}

