﻿using NUnit.Framework;
using System;
using trainer4.Model;

namespace trainer4.Tests
{
	[TestFixture ()]
	public class ChapterStoreTests
	{
		[Test()]
		public void TestAddChapterToDB()
		{
			var cs = new ChapterStore<Chapter> ();

			Chapter ch = new Chapter ();
			ch.ch_desc = "chapter inserted from test method";
			ch.ch_name = "test_chapter_2";
			ch.s_id = 1;
			ch.ch_number = 2;

			cs.AddToDb (ch);
		}
	}
}

