﻿using System;
using NUnit.Framework;
using trainer4.Model;

namespace trainer4.Tests
{
	[TestFixture ()]
	public class ModelTests
	{
		[Test()]
		public void TestContentsCorrectLinkFormat(){
			Contents con = new Contents ();
			con.c_path = "https://www.youtube.com/watch?v=Qq8PTCxuzTA";
			string expectedValue = "https://www.youtube.com/embed/Qq8PTCxuzTA";
			Assert.AreEqual (expectedValue,con.c_path);
			Console.WriteLine (con.c_path);
		}
			
	}
}

