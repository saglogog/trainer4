﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using trainer4.Model;

namespace trainer4.Tests
{
	[TestFixture ()]
	public class RoleStoreTests
	{
		[Test()]
		public async Task TestCreateRoleAsync()
		{
			// Arrange
			var rs = new RoleStore<Role>();

			Role role = new Role
			{
				Name = "student",
				r_desc = "Can take courses and tests and do what a student does"
			};

			// Act
			await rs.CreateAsync(role);

			Task<Role> roleActualTask = rs.FindByNameAsync("student");
			await roleActualTask;
			Role roleActual = roleActualTask.Result;

			// Assert
			Assert.AreEqual(role.Name, roleActual.Name, roleActual.r_desc);
		}

	}
}

