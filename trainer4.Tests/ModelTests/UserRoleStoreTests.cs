﻿using NUnit.Framework;
using System;
using trainer4.Model;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace trainer4.Tests
{
	[TestFixture ()]
	public class UserRoleStoreTests
	{

		[Test()]
		public async Task TestAddUserToRole()
		{
			var urs = new UserRoleStore<Users> ();
			//UserRole urExpected = new UserRole()
			//{
			//	r_id = 1,
			//	u_id = 1
			//};

			Task<Users> userTask = urs.FindByIdAsync("2");
			await userTask;
			await urs.AddToRoleAsync(userTask.Result, "admin");
		}

		[Test()]
		public async Task TestGetUserRoles()
		{
			var urs = new UserRoleStore<Users> ();
			Users user = urs.FindByIdAsync("2").Result;
			List<string> userRolesTask = new List<string>(await urs.GetRolesAsync(user));
			foreach (string result in userRolesTask)
			{
				Console.WriteLine(result);
			}
		}

		[Test()]
		public void TestDoesUserHaveRole()
		{
			var urs = new UserRoleStore<Users> ();
			Console.WriteLine(urs.IsInRoleAsync(urs.FindByIdAsync("2").Result, "admin").Result);
		}

		[Test()]
		public async Task TestRemoveUserFromRole()
		{
			var urs = new UserRoleStore<Users> ();
			await urs.RemoveFromRoleAsync(urs.FindByIdAsync("2").Result, "admin");
		}
	}
}

